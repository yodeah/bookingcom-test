## BOOKING GO TEST

#Installation
- git clone
- npm install 
- npm run start

#Testing
- npm run test (for debugging and watching the files)
- npm run test (for single run on a headless chrome CLI only)

#UI
There is probably an AB test running so I get 2 different ui-s. I was copying the older design.

#Pipeline
I have created a Docker image for node8 and headless chrome, created a small pipeline, where tests ran after every commit. I could have added a deployment step to AWS S3 but did not have time for it.

Test runs
https://bitbucket.org/yodeah/bookingcom-test/addon/pipelines/home#!/

The description of it can be found here:
https://bitbucket.org/yodeah/bookingcom-test/src/master/bitbucket-pipelines.yml


#Consideration
- I did the home component in a reactive way, it's not the most beginner-friendly, readable implementation of the search functionality. Perhaps a better solution  would have been to create more smaller streams, but then the state would be scattered which wouldn’t be as elegant.

- I have created a Factory-Trasformer-Service layer for vertical separation, so the app is more independent of API changes, better testable. In our test-case this is an overkill, but I felt like, this is task to see how I think and develop not whether I’m able to do this task.

- I was also thinking about introducing a store solution (ngRx) but I felt like that would have been too much. If this would be a bigger application communicating  with multiple API-s, and state would have been scattered everywhere It would be a good addition.

- Could have spent more time on design to make it look nicer.

- Usually I write more small unit tests.

#Overall it was a fun task and a nice experience.


