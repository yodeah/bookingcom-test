import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { PickupLocationRequestService } from './service/request/pickup-location-request-service';
import { PickupLocationFactory } from './service/factory/pickup-location-factory';
import { PickupLocationTransformer } from './service/transformer/pickup-location-transformer';
import { DropdownListComponent } from './components/home/dropdown-list/dropdown-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DropdownListComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule
  ],
  providers: [
    PickupLocationRequestService,
    PickupLocationFactory,
    PickupLocationTransformer
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
