import { TestBed, async } from "@angular/core/testing";
import {
  HttpClientTestingModule
} from "@angular/common/http/testing";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./components/home/home.component";
import { PickupLocationFactory } from "./service/factory/pickup-location-factory";
import { DropdownListComponent } from "./components/home/dropdown-list/dropdown-list.component";
import { PickupLocationRequestService } from "./service/request/pickup-location-request-service";
import { PickupLocationTransformer } from "./service/transformer/pickup-location-transformer";

class MockPickupLocationFactory {}

describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent, HomeComponent, DropdownListComponent],
      providers: [
        PickupLocationFactory,
        PickupLocationRequestService,
        PickupLocationTransformer
      ],
      imports: [HttpClientTestingModule]
    }).compileComponents();
  }));

  it(`should host HomeComponent`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("app-home")).toBeTruthy();
  });

  it(`(TEST1 AC1) Given I am a visitor to the rentalcars.com homepage
      Then I should see a Search Widget
      And a text box labelled 'Pick-up Location'
      And the styling as per the rentalcars.com homepage.
  `, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const compiled = fixture.debugElement.nativeElement;
    const inputElement = compiled.querySelector(".yellow-box input");
    const label = compiled.querySelector(".yellow-box p");
    fixture.detectChanges();
    expect(inputElement).toBeTruthy(); //Search widget
    expect(label.innerHTML).toBe("Pick-up Location");
  });

  it(`(TEST1 AC2) Given I am on the Search box within the rentalcars.com homepage
  Then I should see the placeholder text within the 'Pick Up Location' input box: 'city, airport, station, region and district...'
  AND the styling is as per the rentalcars.com homepage
`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const compiled = fixture.debugElement.nativeElement;
    const inputElement = compiled.querySelector(".yellow-box input");
    fixture.detectChanges();
    expect(
      Object.values(inputElement.attributes).filter(
        (attribute: { name: string; nodeValue: string }) =>
          attribute.name === "placeholder"
      )[0]["nodeValue"]
    ).toBe("city, airport, station, region and district...");
  });

  // (TEST1 AC3) Seems to be pretty hard to test from code, it can be a manual test.
  // Highly unlikely that this will change and theres no need for test (Probably can be tested but not a common case)

});
