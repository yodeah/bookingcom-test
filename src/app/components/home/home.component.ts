import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { auditTime, distinctUntilChanged, switchMap, tap, filter } from 'rxjs/operators';
import { PickupLocationFactory } from '../../service/factory/pickup-location-factory';
import { PickupLocation } from '../../service/factory/domain/pickup-location';
import { of } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private readonly resultsToDisplayCount = 6;
  private readonly searchTerm$ = new Subject<string>();

  public resultsToDisplay = new Array<PickupLocation>();
  public message = null;

  constructor(private pickupLocationFactory:PickupLocationFactory) { }

  ngOnInit() {
    this.searchTerm$.pipe(
      auditTime(400),
      distinctUntilChanged(),
      switchMap(
        queryString => this.pickupLocationFactory.getPickupLocations(queryString,this.resultsToDisplayCount)
        .pipe(map( (result:Array<PickupLocation>) => { return {
          queryString:queryString,
          result:result
        }}))
      )
    ).subscribe( this.handleDropdownData, this.handleError );
  }

  private readonly handleDropdownData = (data:{queryString:string, result:Array<PickupLocation>}) => {
    if(data.queryString.length > 1
      && data.result.length > 0
      && data.result[0].name !== "No results found") {
        this.resultsToDisplay = data.result;
        this.message = null;
      }
    else if(data.queryString.length > 1
      && data.result.length === 1
      && data.result[0].name === "No results found") {
        this.message = "No results found"
      }
    else {
      this.resultsToDisplay = [];
      this.message = null;
    }
  }

  private readonly handleError = (err) => this.message ="Error while searching, please reload the page!"

}
