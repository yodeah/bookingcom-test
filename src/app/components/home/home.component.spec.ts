import { async, ComponentFixture, TestBed, tick, fakeAsync } from "@angular/core/testing";

import { HomeComponent } from "./home.component";
import { PickupLocationFactory } from "../../service/factory/pickup-location-factory";
import { Observable, of } from "rxjs";
import { PickupLocation } from "../../service/factory/domain/pickup-location";
import { DropdownListComponent } from "./dropdown-list/dropdown-list.component";

class MockPickupLocationFactory {
  getPickupLocations(  queryString: string, numberOfResults: number ): Observable<Array<PickupLocation>> {
    if ((queryString === "Budapest")) {
      const result = new PickupLocation();
      result.country = "Hungary";
      result.city = "Budapest";
      result.searchType = "G";
      result.alternative = ["HU,Central Hungary"];
      result.placeType = "City";
      result.name = "01. Budavár";
      result.region = "Pest";

      return of([result]);
    }
    const result = new PickupLocation();
    result.name = "No results found";
    return of([result]);
  }
}

describe("HomeComponent", () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent, DropdownListComponent],
      providers: [
        { provide: PickupLocationFactory, useClass: MockPickupLocationFactory }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("(TEST2 AC1) should not have any results when single letter is typed into the searchbox", fakeAsync(() => {
    const compiled = fixture.debugElement.nativeElement;
    const inputElement = compiled.querySelector('input')
    inputElement.value = 'x';
    inputElement.dispatchEvent(new Event('keyup'));
    fixture.detectChanges();
    tick(1000);
    expect(component.resultsToDisplay.length).toBe(0)
  }));
  it("(TEST2 AC1) should not return any results when 'Hello' is typed into the searchbox", fakeAsync(() => {
    const compiled = fixture.debugElement.nativeElement;
    const inputElement = compiled.querySelector('input')
    inputElement.value = 'Hello';
    inputElement.dispatchEvent(new Event('keyup'));
    fixture.detectChanges();
    tick(1000);
    expect(component.resultsToDisplay.length).toBe(0)
  }));
  it("(TEST2 AC2) should return a single result when 'Budapest' is typed into the searchbox", fakeAsync(() => {
    const compiled = fixture.debugElement.nativeElement;
    const inputElement = compiled.querySelector('input')
    inputElement.value = 'Budapest';
    inputElement.dispatchEvent(new Event('keyup'));
    fixture.detectChanges();
    tick(1000);
    expect(component.resultsToDisplay[0].country).toBe('Hungary')
    expect(component.resultsToDisplay[0].placeType).toBe('City')
    expect(component.resultsToDisplay[0].alternative[0]).toBe("HU,Central Hungary")
  }));
  //(TEST2 AC3) Could be tested at higher level component, as we mock out the factory here.
  it("(TEST2 AC4) should have 'No results found' when multiple letters are typed into the searchbox and no search has no result", fakeAsync(() => {
    const compiled = fixture.debugElement.nativeElement;
    const inputElement = compiled.querySelector('input')
    inputElement.value = 'xxxxx';
    inputElement.dispatchEvent(new Event('keyup'));
    fixture.detectChanges();
    tick(1000);
    expect(component.message).toBe('No results found')
  }));
});
