import { Component, OnInit, Input } from '@angular/core';
import { PickupLocation } from '../../../service/factory/domain/pickup-location';

@Component({
  selector: 'app-dropdown-list',
  templateUrl: './dropdown-list.component.html',
  styleUrls: ['./dropdown-list.component.css']
})
export class DropdownListComponent {

  @Input()
  public resultsToDisplay:Array<PickupLocation> = null;

  @Input()
  public message = null;

}
