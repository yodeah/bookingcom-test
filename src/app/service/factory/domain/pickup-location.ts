export class PickupLocation{
  country: string;
  city: string;
  searchType: string;
  alternative: string[];
  placeType: string;
  iata: string;
  name: string;
  region: string;

  constructor(){}
}
