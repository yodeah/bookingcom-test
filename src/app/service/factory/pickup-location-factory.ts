import { PickupLocationRequestService } from "../request/pickup-location-request-service";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { PickupLocation } from "./domain/pickup-location";
import { PickupLocationTransformer } from "../transformer/pickup-location-transformer";
import { map } from "rxjs/internal/operators/map";

@Injectable()
export class PickupLocationFactory{

  constructor(
    private pickupLocationRequestService:PickupLocationRequestService,
    private pickupLocationTransformer:PickupLocationTransformer
    ){ }

  public getPickupLocations(queryString:string, numberOfResults:number):Observable<Array<PickupLocation>>{
    return this.pickupLocationRequestService.queryPickupLocations(queryString,numberOfResults).pipe(
      map( results => this.pickupLocationTransformer.responseToPickupLocation(results))
    );
  }


}
