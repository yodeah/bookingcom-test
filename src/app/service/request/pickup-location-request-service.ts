import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/internal/Observable";
import { PickupLocationsResponse } from "./domain/pickup-location-response";

@Injectable()
export class PickupLocationRequestService{

  constructor(private http: HttpClient) { }

  public queryPickupLocations(queryString:string, numberOfResults:number):Observable<PickupLocationsResponse>{
    const url = "https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=" + numberOfResults.toString() + "&solrTerm=" + queryString
    return this.http.get<PickupLocationsResponse>(url);
  }

}
