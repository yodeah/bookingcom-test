export class PickupLocationsResponse {
  results: {
    isGooglePowered: boolean;
    numFound:number;
    docs: Array<PickupLocationsResponseItem>;
  };
}

export class PickupLocationsResponseItem{
  country: string;
  lng: number;
  city: string;
  searchType: string;
  alternative: string[];
  bookingId: string;
  placeType: string;
  placeKey: string;
  iata: string;
  countryIso: string;
  locationId: string;
  name: string;
  ufi: number;
  isPopular: boolean;
  region: string;
  lang: string;
  lat: number;
}
