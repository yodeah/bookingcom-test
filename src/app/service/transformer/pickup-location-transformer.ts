import { PickupLocationsResponse, PickupLocationsResponseItem } from "../request/domain/pickup-location-response";
import { PickupLocation } from "../factory/domain/pickup-location";
import { Injectable } from "@angular/core";

@Injectable()
export class PickupLocationTransformer{

  public responseToPickupLocation(pickupLocationResponse:PickupLocationsResponse):Array<PickupLocation> {
    if(!!pickupLocationResponse && !!pickupLocationResponse.results && !!pickupLocationResponse.results.docs){
      return pickupLocationResponse.results.docs.map( (pickupLocationsResponseItem:PickupLocationsResponseItem) => {
        const pickupLocation = new PickupLocation();
        pickupLocation.country = pickupLocationsResponseItem.country;
        pickupLocation.city = pickupLocationsResponseItem.city;
        pickupLocation.searchType = pickupLocationsResponseItem.searchType;
        pickupLocation.alternative = pickupLocationsResponseItem.alternative;
        pickupLocation.placeType = this.getTypeByShorthand(pickupLocationsResponseItem.placeType);
        pickupLocation.iata = pickupLocationsResponseItem.iata;
        pickupLocation.name = pickupLocationsResponseItem.name;
        pickupLocation.region = pickupLocationsResponseItem.region;

        return pickupLocation;
      }
     );
    }
    return [];
  }

  private getTypeByShorthand(short:string):string{
    switch(short){
      case "A": return "Airport"
      case "C": return "City"
      case "S": return "Station"
      case "T": return "Train Station"
      default: return "City"
    }
  }

}
